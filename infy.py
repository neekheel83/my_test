import requests
import json

# Define the stock symbol
symbol = "INFY"

# Define the API endpoint
#endpoinit = f"https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey=S0S2X9ONF0AHDWY8"
endpoint = f"https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey=YOUR_API_KEY"


# Send a GET request to the API endpoint
response = requests.get(endpoint)

# Parse the JSON response
data = json.loads(response.text)

# Extract the relevant information from the response
price = data["Global Quote"]["05. price"]
change = data["Global Quote"]["09. change"]
change_percent = data["Global Quote"]["10. change percent"]

# Print the stock information
print(f"Stock Symbol: {symbol}")
print(f"Price: {price}")
print(f"Change: {change}")
print(f"Change Percent: {change_percent}")

